package us.astar.loderunner;

import ca.umontreal.iro.hackathon.loderunner.Direction;

public class Coordinates {

	private final int x, y;

	public Coordinates(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Coordinates moveLeft() {
		if (x <= 0) {
			return null;
		}

		return new Coordinates(x - 1, y);
	}

	public Coordinates moveUp() {
		if (y <= 0) {
			return null;
		}

		return new Coordinates(x, y - 1);
	}

	public Coordinates moveRight(int sizeX) {
		if (x >= (sizeX - 1)) {
			return null;
		}

		return new Coordinates(x + 1, y);
	}

	public Coordinates moveDown(int sizeY) {
		if (y >= (sizeY - 1)) {
			return null;
		}

		return new Coordinates(x, y + 1);
	}

	public Direction getDirectionTo(Coordinates other) {
		int diffX = other.x - this.x;
		if (diffX > 0) {
			return Direction.RIGHT;
		}

		if (diffX < 0) {
			return Direction.LEFT;
		}

		int diffY = other.y - this.y;
		if (diffY > 0) {
			return Direction.DOWN;
		}

		if (diffY < 0) {
			return Direction.UP;
		}

		return Direction.NONE;
	}

	public int taxiDistance(Coordinates other) {
		return Math.abs(this.x - other.x) + Math.abs(this.y - other.y);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + this.x;
		hash = 31 * hash + this.y;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Coordinates)) {
			return false;
		}

		Coordinates other = (Coordinates) obj;
		return this.x == other.x && this.y == other.y;
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ')';
	}

}
