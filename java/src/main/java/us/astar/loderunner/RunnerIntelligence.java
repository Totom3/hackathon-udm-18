package us.astar.loderunner;

import ca.umontreal.iro.hackathon.loderunner.Direction;
import ca.umontreal.iro.hackathon.loderunner.Event;
import ca.umontreal.iro.hackathon.loderunner.Move;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class RunnerIntelligence {

	public static Comparator<Coordinates> smallestDistance(Coordinates coords) {
		return (a, b) -> {
			int distA = a.taxiDistance(coords);
			int distB = b.taxiDistance(coords);
			return distB - distA;
		};
	}

	public static Coordinates findClosestTo(Coordinates goal, List<Coordinates> list) {
		if (list.isEmpty()) {
			return null;
		}

		Coordinates closest = null;
		for (Coordinates c : list) {
			if (closest == null || c.taxiDistance(goal) < closest.taxiDistance(goal)) {
				closest = c;
			}
		}

		return closest;
	}

	private final char[][] level;
	private final int sizeX, sizeY;

	private Coordinates startPosition;
	private Coordinates endPosition;
	private final List<Coordinates> goldPieces;

	private RunnerPath currentPath;
	private Coordinates currentGoal;

	private Iterator<RunnerPath> pathIterator;

	public RunnerIntelligence(char[][] level) {
		this.level = level;
		this.sizeY = level.length;
		this.sizeX = level[0].length;
		this.goldPieces = new ArrayList<>();
	}

	public void parseLevel() {
		for (int y = 0; y < sizeY; ++y) {
			for (int x = 0; x < sizeX; ++x) {
				char ch = level[y][x];
				if (ch == BlockCodes.GOLD) {
					level[y][x] = ' ';
					goldPieces.add(new Coordinates(x, y));
				}

				if (ch == BlockCodes.START) {
					level[y][x] = ' ';
					startPosition = new Coordinates(x, y);
				}

				if (ch == BlockCodes.EXIT) {
					level[y][x] = ' ';
					endPosition = new Coordinates(x, y);
				}
			}
		}

		System.out.println("Start: " + startPosition + "; End: " + endPosition + "; Gold: " + goldPieces);
	}

	public void computePaths() {
		computePaths(startPosition);
	}

	public void computePaths(Coordinates from) {
		List<RunnerPath> paths = new ArrayList<>(goldPieces.size());
		boolean success = Util.computePaths(level, sizeX, sizeY, from, paths, new ArrayList<>(goldPieces), endPosition);
		if (!success) {
			currentPath = null;
			return;
		}

		pathIterator = paths.iterator();
		currentPath = pathIterator.next();
	}

	public Move move(int x, int y) {
		Coordinates currentPosition = new Coordinates(x, y);
		if (currentPath == null) {
			if (getBlockCode(currentPosition) == BlockCodes.BRICK) {
				level[y][x] = BlockCodes.AIR;
			}
			computePaths(currentPosition);
			return new Move(Event.MOVE, Direction.NONE);
		}

		// Find new path
		if (currentPath.hasFailed()) {
			scanFakeBricks(x, y);

			if (goldPieces.isEmpty()) {
				// Go for the exit
				currentGoal = endPosition;
				System.out.println("Trying again: exit");
				currentPath = new RunnerPath(currentGoal, performSearch(currentPosition, endPosition, Integer.MAX_VALUE));
			} else {
				if (getBlockCode(currentPosition) == BlockCodes.BRICK) {
					level[y][x] = BlockCodes.AIR;
				}
				computePaths(currentPosition);
				if (currentPath == null) {
					return new Move(Event.MOVE, Direction.NONE);
				}
			}

		} else if (currentPath.isDone()) {
			goldPieces.remove(currentPath.getGoal());

			if (pathIterator.hasNext()) {
				currentPath = pathIterator.next();
				currentGoal = currentPath.getGoal();
				System.out.println("New goal: " + currentGoal);
			} else {
				currentGoal = endPosition;
				System.out.println("New goal: exit");
				currentPath = new RunnerPath(currentGoal, performSearch(currentPosition, endPosition, Integer.MAX_VALUE));
			}

		}

		return currentPath.nextAction(x, y);
	}

	private void scanFakeBricks(int x, int y) {
		Coordinates coords = new Coordinates(x, y);
		if (getBlockCode(coords) == BlockCodes.AIR) {
			coords = coords.moveUp();
		}

		while (getBlockCode(coords) == BlockCodes.BRICK) {
			level[coords.getY()][coords.getX()] = BlockCodes.AIR;
			coords = coords.moveUp();
			System.out.println("Detected fake brick at " + coords);
		}
	}

	// --------------------------------
	private char getBlockCode(Coordinates coords) {
		if (coords == null) {
			return 0;
		}

		return level[coords.getY()][coords.getX()];
	}

	private List<Coordinates> performSearch(Coordinates from, Coordinates to, int max) {
		PathSearcher searcher = new PathSearcher(from, to, level, sizeX, sizeY);
		return searcher.performSearch(max);
	}
}
