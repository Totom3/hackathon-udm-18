package us.astar.loderunner;

import ca.umontreal.iro.hackathon.loderunner.Direction;
import ca.umontreal.iro.hackathon.loderunner.Event;
import ca.umontreal.iro.hackathon.loderunner.Move;
import java.util.List;
import java.util.ListIterator;

public class RunnerPath {

	private final Coordinates goal;
	private final List<Coordinates> coordinates;

	private boolean failed;
	private Coordinates currentPosition;
	private ListIterator<Coordinates> iterator;

	public RunnerPath(Coordinates goal, List<Coordinates> coordinates) {
		this.goal = goal;
		this.coordinates = coordinates;
		this.iterator = coordinates.listIterator();
		this.currentPosition = iterator.next();
	}

	public int size() {
		return coordinates.size();
	}

	public List<Coordinates> getPath() {
		return coordinates;
	}

	public Coordinates getGoal() {
		return goal;
	}

	public Coordinates getPathEnd() {
		return coordinates.get(coordinates.size() - 1);
	}

	public boolean isDone() {
		return !iterator.hasNext();
	}

	public boolean hasFailed() {
		return failed;
	}

	public Move nextAction(int x, int y) {
		Coordinates pos = new Coordinates(x, y);
		while (!currentPosition.equals(pos) && iterator.hasNext()) {
			// fast forward until we find the right node
			currentPosition = iterator.next();
		}

		while (!currentPosition.equals(pos) && iterator.hasPrevious()) {
			// look back?? (it somehow works, don't even ask!)
			currentPosition = iterator.previous();
		}

		// we have not found the node
		if (!currentPosition.equals(pos)) {
			failed = true;
			return new Move(Event.MOVE, Direction.NONE);
		}

		if (!iterator.hasNext()) {
			return new Move(Event.MOVE, Direction.NONE);
		}

		Coordinates oldPosition = currentPosition;
		this.currentPosition = iterator.next();

		if (currentPosition instanceof DigCoordinates) {
			DigCoordinates d = (DigCoordinates) currentPosition;
			return new Move(Event.DIG, d.isLeft() ? Direction.LEFT : Direction.RIGHT);
		}

		//if (iterator.previousIndex() == 0 && currentPosition.equals(oldPosition)) {
		//		if (iterator.hasNext()) {
//				currentPosition = iterator.next();
		//		} else {
		//			return new Move(Event.MOVE, Direction.NONE);
		//		}
	//}

	return new Move(Event.MOVE, oldPosition.getDirectionTo

(currentPosition));
	}
}
