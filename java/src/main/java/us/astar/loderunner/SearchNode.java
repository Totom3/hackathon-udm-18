package us.astar.loderunner;

public class SearchNode implements Comparable<SearchNode> {

	private final Coordinates coordinates;

	private SearchNode parent;
	private int pathDistance;
	private final int heuristic;

	public SearchNode(Coordinates coords, Coordinates goal) {
		this.coordinates = coords;
		this.heuristic = coords.taxiDistance(goal);
		this.pathDistance = Integer.MAX_VALUE;
	}

	public SearchNode(Coordinates coords, Coordinates goal, int distance, SearchNode parent) {
		this.coordinates = coords;
		this.heuristic = coords.taxiDistance(goal);
		this.pathDistance = distance;
		this.parent = parent;
	}

	public void updateBestPath(int distance, SearchNode parent) {
		// it somehow overflows sometimes
		if (this.pathDistance < 0 || this.pathDistance > distance) {
			this.pathDistance = distance;
			this.parent = parent;
		}
	}

	public void setStart() {
		this.parent = null;
		this.pathDistance = 0;
	}

	public int getPathDistance() {
		return pathDistance;
	}

	public int getHeuristic() {
		return heuristic;
	}

	public int getCost() {
		return pathDistance + heuristic;
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public SearchNode getParent() {
		return parent;
	}

	@Override
	public int compareTo(SearchNode o) {
		return getHeuristic() - o.getHeuristic();
	}

}
