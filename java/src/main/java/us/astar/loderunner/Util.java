package us.astar.loderunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Util {

	private static final Comparator<RunnerPath> COMPARATOR = (a, b) -> a.size() - b.size();

	public static boolean computePaths(char[][] level, int sizeX, int sizeY, Coordinates from, List<RunnerPath> paths, List<Coordinates> remaining, Coordinates exit) {
		if (remaining.isEmpty()) {
			if (exit == null) {
				return true;
			}

			PathSearcher searcher = new PathSearcher(from, exit, level, sizeX, sizeY);
			List<Coordinates> path = searcher.performSearch(Integer.MAX_VALUE);
			if (path != null) {
				paths.add(new RunnerPath(exit, path));
				return true;
			} else {
				return false;
			}
		}

		List<RunnerPath> newPaths = getPaths(level, sizeX, sizeY, from, remaining);
		if (newPaths.isEmpty()) {
			return false;
		}

		Collections.sort(newPaths, COMPARATOR);

		for (RunnerPath path : newPaths) {
			paths.add(path);
			remaining.remove(path.getGoal());
			if (computePaths(level, sizeX, sizeY, path.getPathEnd(), paths, remaining, exit)) {
				return true;
			}
			
			paths.remove(path);
			remaining.add(path.getGoal());
		}

		return false;
	}

	private static List<RunnerPath> getPaths(char[][] level, int sizeX, int sizeY, Coordinates from, List<Coordinates> objectives) {
		List<RunnerPath> paths = new ArrayList<>(objectives.size());
		for (Coordinates obj : objectives) {
			PathSearcher searcher = new PathSearcher(from, obj, level, sizeX, sizeY);
			List<Coordinates> path = searcher.performSearch(Integer.MAX_VALUE);
			if (path == null) {
				continue;
			}

			paths.add(new RunnerPath(obj, path));
		}

		return paths;
	}
}
