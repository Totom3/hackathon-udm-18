package us.astar.loderunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class PathSearcher {

	private final SearchNode[][] nodes;
	private final SearchNode start, end;

	private final char[][] level;
	private final int sizeX, sizeY;

	public PathSearcher(Coordinates startCoords, Coordinates endCoords, char[][] level, int sizeX, int sizeY) {
		this.start = new SearchNode(startCoords, endCoords);
		this.end = new SearchNode(endCoords, endCoords);

		this.level = level;
		this.sizeX = sizeX;
		this.sizeY = sizeY;

		this.nodes = new SearchNode[sizeY][sizeX];
		nodes[startCoords.getY()][startCoords.getX()] = start;
		nodes[endCoords.getY()][endCoords.getX()] = end;
	}

	public List<Coordinates> performSearch(int max) {
		Set<SearchNode> alreadyVisited = new HashSet<>();
		PriorityQueue<SearchNode> toBeVisited = new PriorityQueue() {
			@Override
			public boolean add(Object o) {
				SearchNode n = (SearchNode) o;
				if (n.getPathDistance() < 0 || n.getPathDistance() >= Integer.MAX_VALUE) {
					throw new AssertionError();
				}
				return super.add(o);
			}

		};
		start.setStart();
		toBeVisited.add(start);

		int bestDistance = max;
		SearchNode actualEnd = end;

		outer:
		while (!toBeVisited.isEmpty()) {
			SearchNode node = toBeVisited.poll();
			if (!alreadyVisited.add(node)) {
				continue;
			}

			int newDistance = 1 + node.getPathDistance();
			if (newDistance >= bestDistance) {
				continue;
			}

			// check for moves
			for (Coordinates coords : getAdjacent(node.getCoordinates())) {
				SearchNode neighbor = node(coords);
				neighbor.updateBestPath(newDistance, node);
				if (coords.equals(end.getCoordinates())) {
					bestDistance = newDistance;
					continue outer;
				}

				if (!alreadyVisited.contains(neighbor) && !toBeVisited.contains(neighbor)) {
					toBeVisited.add(neighbor);
				}
			}

			// check for one layer dig
			Coordinates pos = node.getCoordinates();
			if (pos instanceof DigCoordinates) {
				continue;
			}

			checkForOneLayerDig(pos, newDistance, node, toBeVisited);
			checkForTwoLayerLeftDig(pos, newDistance, node, toBeVisited);
			checkForTwoLayerRightDig(pos, newDistance, node, toBeVisited);

			SearchNode exit = checkForLateralLeftDig(pos, newDistance, node, toBeVisited);
			if (exit != null) {
				// Restrictive left dig that finds the solution
				actualEnd = exit;
				bestDistance = exit.getPathDistance();
				break outer;
			}

			exit = checkForLateralRightDig(pos, newDistance, node, toBeVisited);
			if (exit != null) {
				// Restrictive left dig that finds the solution
				actualEnd = exit;
				bestDistance = exit.getPathDistance();
				break outer;
			}
		}

		// found no path
		if (bestDistance == Integer.MAX_VALUE) {
			return null;
		}

		// reconstruct path
		LinkedList<Coordinates> path = new LinkedList<>();
		SearchNode node = actualEnd;
		do {
			path.addFirst(node.getCoordinates());
			if (node == start) {
				break;
			}

			node = node.getParent();
		} while (node != null);

		return path;
	}

	private void checkForOneLayerDig(Coordinates pos, int newDistance, SearchNode node, PriorityQueue<SearchNode> toBeVisited) {
		if (pos.getY() >= sizeY - 3) {
			return;
		}

		if (!BlockCodes.isWalkable(pos.moveDown(sizeY), level)) {
			return;
		}

		// check left dig
		Coordinates c = pos.moveDown(sizeY).moveLeft();
		Coordinates c1 = pos.moveLeft();
		if (c != null && c1 != null && getBlockCode(c) == BlockCodes.BRICK && BlockCodes.isPassable(c.moveDown(sizeY), level) && BlockCodes.isPassable(c1, level)) {
			// dig left
			DigCoordinates dig = new DigCoordinates(pos.getX(), pos.getY(), true);
			Coordinates left = pos.moveLeft();
			Coordinates leftDown = left.moveDown(sizeY);

			SearchNode node1 = new SearchNode(dig, end.getCoordinates(), newDistance, node);
			SearchNode node2 = new SearchNode(left, end.getCoordinates(), newDistance + 1, node1);
			SearchNode node3 = new SearchNode(leftDown, end.getCoordinates(), newDistance + 2, node2);

			if (nodes[leftDown.getY()][leftDown.getX()] == null) {
				nodes[leftDown.getY()][leftDown.getX()] = node3;
			}

			toBeVisited.add(node3);
		}

		// check right dig
		c = pos.moveDown(sizeY).moveRight(sizeX);
		c1 = pos.moveRight(sizeX);
		if (c != null && c1 != null && getBlockCode(c) == BlockCodes.BRICK && BlockCodes.isPassable(c.moveDown(sizeY), level) && BlockCodes.isPassable(c1, level)) {
			// dig right
			DigCoordinates dig = new DigCoordinates(pos.getX(), pos.getY(), false);
			Coordinates right = pos.moveRight(sizeX);
			Coordinates rightDown = right.moveDown(sizeY);

			SearchNode node1 = new SearchNode(dig, end.getCoordinates(), newDistance, node);
			SearchNode node2 = new SearchNode(right, end.getCoordinates(), newDistance + 1, node1);
			SearchNode node3 = new SearchNode(rightDown, end.getCoordinates(), newDistance + 2, node2);

			if (nodes[rightDown.getY()][rightDown.getX()] == null) {
				nodes[rightDown.getY()][rightDown.getX()] = node3;
			}

			toBeVisited.add(node3);
		}
	}

	private void checkForTwoLayerLeftDig(Coordinates pos, int newDistance, SearchNode node, PriorityQueue<SearchNode> toBeVisited) {
		if (pos.getY() >= sizeY - 4) {
			return;
		}

		// condition 1: be able to move right
		if (!BlockCodes.isPassable(pos.moveRight(sizeX), level)
				|| !BlockCodes.isWalkable(pos.moveRight(sizeX).moveDown(sizeY), level)) {
			return;
		}

		// condition 2 - 4: stand on bricks
		Coordinates c = pos.moveDown(sizeY);
		if (getBlockCode(c) != BlockCodes.BRICK || getBlockCode(c.moveLeft()) != BlockCodes.BRICK || getBlockCode(c.moveLeft().moveDown(sizeY)) != BlockCodes.BRICK) {
			return;
		}

		// condition 5: passable block under the bricks
		if (!BlockCodes.isPassable(c.moveLeft().moveDown(sizeY).moveDown(sizeY), level)) {
			return;
		}

		// we can do a double left dig!
		DigCoordinates dig = new DigCoordinates(pos.getX(), pos.getY(), true);
		Coordinates right = pos.moveRight(sizeX);
		DigCoordinates dig2 = new DigCoordinates(right.getX(), right.getY(), true);
		Coordinates down = pos.moveDown(sizeY);
		DigCoordinates dig3 = new DigCoordinates(down.getX(), down.getY(), true);
		Coordinates downLeft = down.moveLeft();
		Coordinates downLeftDown = downLeft.moveDown(sizeY);
		Coordinates downLeftDownDown = downLeftDown.moveDown(sizeY);
		SearchNode node1 = new SearchNode(dig, end.getCoordinates(), newDistance, node);
		SearchNode node2 = new SearchNode(right, end.getCoordinates(), newDistance + 1, node1);
		SearchNode node3 = new SearchNode(dig2, end.getCoordinates(), newDistance + 2, node2);
		SearchNode node4 = new SearchNode(pos, end.getCoordinates(), newDistance + 3, node3);
		SearchNode node5 = new SearchNode(down, end.getCoordinates(), newDistance + 4, node4);
		SearchNode node6 = new SearchNode(dig3, end.getCoordinates(), newDistance + 5, node5);
		SearchNode node7 = new SearchNode(downLeft, end.getCoordinates(), newDistance + 6, node6);
		SearchNode node8 = new SearchNode(downLeftDown, end.getCoordinates(), newDistance + 7, node7);
		SearchNode node9 = new SearchNode(downLeftDownDown, end.getCoordinates(), newDistance + 8, node8);

		if (nodes[downLeftDownDown.getY()][downLeftDownDown.getX()] == null) {
			nodes[downLeftDownDown.getY()][downLeftDownDown.getX()] = node9;
		}

		toBeVisited.add(node9);
	}

	private void checkForTwoLayerRightDig(Coordinates pos, int newDistance, SearchNode node, PriorityQueue<SearchNode> toBeVisited) {
		if (pos.getY() >= sizeY - 4) {
			return;
		}

		// condition 1: be able to move left
		if (!BlockCodes.isPassable(pos.moveLeft(), level)
				|| !BlockCodes.isWalkable(pos.moveLeft().moveDown(sizeY), level)) {
			return;
		}

		// condition 2 - 4: stand on bricks
		Coordinates c = pos.moveDown(sizeY);
		if (getBlockCode(c) != BlockCodes.BRICK || getBlockCode(c.moveRight(sizeX)) != BlockCodes.BRICK || getBlockCode(c.moveRight(sizeX).moveDown(sizeY)) != BlockCodes.BRICK) {
			return;
		}

		// condition 5: passable block under the bricks
		if (!BlockCodes.isPassable(c.moveRight(sizeX).moveDown(sizeY).moveDown(sizeY), level)) {
			return;
		}

		// we can do a double right dig!
		DigCoordinates dig = new DigCoordinates(pos.getX(), pos.getY(), false);
		Coordinates left = pos.moveLeft();
		DigCoordinates dig2 = new DigCoordinates(left.getX(), left.getY(), false);
		Coordinates down = pos.moveDown(sizeY);
		DigCoordinates dig3 = new DigCoordinates(down.getX(), down.getY(), false);
		Coordinates downRight = down.moveRight(sizeX);
		Coordinates downRightDown = downRight.moveDown(sizeY);
		Coordinates downRightDownDown = downRightDown.moveDown(sizeY);
		SearchNode node1 = new SearchNode(dig, end.getCoordinates(), newDistance, node);
		SearchNode node2 = new SearchNode(left, end.getCoordinates(), newDistance + 1, node1);
		SearchNode node3 = new SearchNode(dig2, end.getCoordinates(), newDistance + 2, node2);
		SearchNode node4 = new SearchNode(pos, end.getCoordinates(), newDistance + 3, node3);
		SearchNode node5 = new SearchNode(down, end.getCoordinates(), newDistance + 4, node4);
		SearchNode node6 = new SearchNode(dig3, end.getCoordinates(), newDistance + 5, node5);
		SearchNode node7 = new SearchNode(downRight, end.getCoordinates(), newDistance + 6, node6);
		SearchNode node8 = new SearchNode(downRightDown, end.getCoordinates(), newDistance + 7, node7);
		SearchNode node9 = new SearchNode(downRightDownDown, end.getCoordinates(), newDistance + 8, node8);

		if (nodes[downRightDownDown.getY()][downRightDownDown.getX()] == null) {
			nodes[downRightDownDown.getY()][downRightDownDown.getX()] = node9;
		}

		toBeVisited.add(node9);
	}

	private SearchNode checkForLateralLeftDig(Coordinates pos, int newDistance, SearchNode node, PriorityQueue<SearchNode> toBeVisited) {
		Coordinates down = pos.moveDown(sizeY);
		if (getBlockCode(down) != BlockCodes.LADDER) {
			return null;
		}

		Coordinates downLeft = down.moveLeft();
		if (getBlockCode(downLeft) != BlockCodes.BRICK) {
			return null;
		}

		Coordinates downLeftLeft = downLeft.moveLeft();
		if (!BlockCodes.isPassable(downLeftLeft, level)) {
			return null;
		}

		// Check for a restrictive lateral dig
		// condition 1: the goal is one block lower and less than 3 to the left
		// condition 2: scanning left will return a ladder/rope down or passable downdown before a non-passable down
		int diffY = end.getCoordinates().getY() - pos.getY();
		int diffX = end.getCoordinates().getX() - pos.getX();
		if (diffY == 1 && diffX > -4 && diffX < -1 && restrictiveLeftScan(end.getCoordinates())) {
			Coordinates downLeftLeftLeft = downLeftLeft.moveLeft();

			// Restrictive left lateral dig; take appropriate measures to not get stuck
			DigCoordinates dig = new DigCoordinates(pos.getX(), pos.getY(), true);
			SearchNode node1 = new SearchNode(dig, end.getCoordinates(), newDistance, node);
			SearchNode node2 = new SearchNode(down, end.getCoordinates(), newDistance + 1, node1);
			SearchNode node3 = new SearchNode(downLeft, end.getCoordinates(), newDistance + 2, node2);
			SearchNode node4 = new SearchNode(downLeftLeft, end.getCoordinates(), newDistance + 3, node3);
			SearchNode node5 = new SearchNode(downLeftLeftLeft, end.getCoordinates(), newDistance + 4, node4);
			SearchNode node6 = new SearchNode(downLeftLeft, end.getCoordinates(), newDistance + 5, node5);
			SearchNode node7 = new SearchNode(downLeft, end.getCoordinates(), newDistance + 6, node6);
			SearchNode node8 = new SearchNode(down, end.getCoordinates(), newDistance + 7, node7);
			return node8;
		}

		// left lateral dig is possible!
		DigCoordinates dig = new DigCoordinates(pos.getX(), pos.getY(), true);
		SearchNode node1 = new SearchNode(dig, end.getCoordinates(), newDistance, node);
		SearchNode node2 = new SearchNode(down, end.getCoordinates(), newDistance + 1, node1);
		SearchNode node3 = new SearchNode(downLeft, end.getCoordinates(), newDistance + 2, node2);
		SearchNode node4 = new SearchNode(downLeftLeft, end.getCoordinates(), newDistance + 3, node3);
		if (nodes[downLeftLeft.getY()][downLeftLeft.getX()] == null) {
			nodes[downLeftLeft.getY()][downLeftLeft.getX()] = node4;
		}

		toBeVisited.add(node4);
		return null;
	}

	private SearchNode checkForLateralRightDig(Coordinates pos, int newDistance, SearchNode node, PriorityQueue<SearchNode> toBeVisited) {
		Coordinates down = pos.moveDown(sizeY);
		if (getBlockCode(down) != BlockCodes.LADDER) {
			return null;
		}

		Coordinates downRight = down.moveRight(sizeX);
		if (getBlockCode(downRight) != BlockCodes.BRICK) {
			return null;
		}

		Coordinates downRightRight = downRight.moveRight(sizeX);
		if (!BlockCodes.isPassable(downRightRight, level)) {
			return null;
		}

		// Check for a restrictive lateral dig
		// condition 1: the goal is one block lower and less than 3 to the right
		// condition 2: scanning right will return a ladder/rope down or passable downdown before a non-passable down
		int diffY = end.getCoordinates().getY() - pos.getY();
		int diffX = end.getCoordinates().getX() - pos.getX();
		if (diffY == 1 && diffX < 4 && diffX > 1 && restrictiveRightScan(end.getCoordinates())) {
			Coordinates downRightRightRight = downRightRight.moveRight(sizeX);

			// Restrictive right lateral dig; take appropriate measures to not get stuck
			DigCoordinates dig = new DigCoordinates(pos.getX(), pos.getY(), false);
			SearchNode node1 = new SearchNode(dig, end.getCoordinates(), newDistance, node);
			SearchNode node2 = new SearchNode(down, end.getCoordinates(), newDistance + 1, node1);
			SearchNode node3 = new SearchNode(downRight, end.getCoordinates(), newDistance + 2, node2);
			SearchNode node4 = new SearchNode(downRightRight, end.getCoordinates(), newDistance + 3, node3);
			SearchNode node5 = new SearchNode(downRightRightRight, end.getCoordinates(), newDistance + 4, node4);
			SearchNode node6 = new SearchNode(downRightRight, end.getCoordinates(), newDistance + 5, node5);
			SearchNode node7 = new SearchNode(downRight, end.getCoordinates(), newDistance + 6, node6);
			SearchNode node8 = new SearchNode(down, end.getCoordinates(), newDistance + 7, node7);
			return node8;
		}

		// right lateral dig is possible!
		DigCoordinates dig = new DigCoordinates(pos.getX(), pos.getY(), false);
		SearchNode node1 = new SearchNode(dig, end.getCoordinates(), newDistance, node);
		SearchNode node2 = new SearchNode(down, end.getCoordinates(), newDistance + 1, node1);
		SearchNode node3 = new SearchNode(downRight, end.getCoordinates(), newDistance + 2, node2);
		SearchNode node4 = new SearchNode(downRightRight, end.getCoordinates(), newDistance + 3, node3);
		if (nodes[downRightRight.getY()][downRightRight.getX()] == null) {
			nodes[downRightRight.getY()][downRightRight.getX()] = node4;
		}

		toBeVisited.add(node4);
		return null;
	}

	private boolean restrictiveLeftScan(Coordinates coords) {
		do {
			char ch = getBlockCode(coords);
			switch (ch) {
				case BlockCodes.ROCK:
				case BlockCodes.BRICK:
					// restrictive
					return true;
				case BlockCodes.LADDER:
				case BlockCodes.ROPE:
					// nonrestrictive
					return false;
			}

			Coordinates down = coords.moveDown(sizeY);
			if (BlockCodes.isPassable(down, level)) {
				return false; // nonrestrictive
			}
			coords = coords.moveLeft();
		} while (coords != null);

		return true;
	}

	private boolean restrictiveRightScan(Coordinates coords) {
		do {
			char ch = getBlockCode(coords);
			switch (ch) {
				case BlockCodes.ROCK:
				case BlockCodes.BRICK:
					// restrictive
					return true;
				case BlockCodes.LADDER:
				case BlockCodes.ROPE:
					// nonrestrictive
					return false;
			}

			Coordinates down = coords.moveDown(sizeY);
			if (BlockCodes.isPassable(down, level)) {
				return false; // nonrestrictive
			}
			coords = coords.moveRight(sizeX);
		} while (coords != null);

		return true;
	}

	private SearchNode node(Coordinates coords) {
		SearchNode node = nodes[coords.getY()][coords.getX()];
		if (node == null) {
			node = new SearchNode(coords, end.getCoordinates());
			nodes[coords.getY()][coords.getX()] = node;
		}

		return node;
	}

	private char getBlockCode(Coordinates coords) {
		if (coords == null) {
			return 0;
		}

		return level[coords.getY()][coords.getX()];
	}

	private List<Coordinates> getAdjacent(Coordinates coords) {
		char ch = getBlockCode(coords);
		List<Coordinates> adjacent = new ArrayList<>(4);

		switch (ch) {
			case BlockCodes.BRICK:
			case BlockCodes.ROCK:
				addAdjacentDown(coords, adjacent);
				return adjacent;
			// rope
			case BlockCodes.ROPE:
				addAdjacentLeft(coords, adjacent);
				addAdjacentRight(coords, adjacent);
				addAdjacentDown(coords, adjacent);
				return adjacent;
			// ladder
			case BlockCodes.LADDER:
				addAdjacentDown(coords, adjacent);
				addAdjacentUp(coords, adjacent);
				addAdjacentLeft(coords, adjacent);
				addAdjacentRight(coords, adjacent);

				return adjacent;
			// air
			case BlockCodes.AIR:
				boolean standingOnSolid = BlockCodes.isWalkable(coords.moveDown(sizeY), level);
				if (standingOnSolid) {
					addAdjacentLeft(coords, adjacent);
					addAdjacentRight(coords, adjacent);
					if (getBlockCode(coords.moveUp()) == BlockCodes.LADDER) {
						adjacent.add(coords.moveUp());
					}
				} else {
					addAdjacentDown(coords, adjacent);
				}
				return adjacent;
			default:
				throw new AssertionError("Unexpected block type '" + ch + "'");
		}
	}

	private void addAdjacentUp(Coordinates coords, List<Coordinates> adjacent) {
		if (BlockCodes.isPassable(coords.moveUp(), level)) {
			adjacent.add(coords.moveUp());
		}
	}

	private void addAdjacentDown(Coordinates coords, List<Coordinates> adjacent) {
		if (BlockCodes.isPassable(coords.moveDown(sizeY), level)) {
			adjacent.add(coords.moveDown(sizeY));
		}
	}

	private void addAdjacentRight(Coordinates coords, List<Coordinates> adjacent) {
		if (BlockCodes.isPassable(coords.moveRight(sizeX), level)) {
			adjacent.add(coords.moveRight(sizeX));
		}
	}

	private void addAdjacentLeft(Coordinates coords, List<Coordinates> adjacent) {
		if (BlockCodes.isPassable(coords.moveLeft(), level)) {
			adjacent.add(coords.moveLeft());
		}
	}

}
