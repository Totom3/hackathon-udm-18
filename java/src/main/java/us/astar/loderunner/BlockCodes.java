package us.astar.loderunner;

public class BlockCodes {

	public static final char AIR = ' ';
	public static final char ROPE = '-';
	public static final char LADDER = 'H';
	public static final char GOLD = '$';
	public static final char START = '&';
	public static final char EXIT = 'S';
	public static final char ROCK = '@';
	public static final char BRICK = '#';

	public static boolean isPassable(char ch) {
		switch (ch) {
			case AIR:
			case ROPE:
			case LADDER:
			case GOLD:
			case START:
			case EXIT:
				return true;
			default:
				return false;
		}
	}

	public static boolean isWalkable(char ch) {
		switch (ch) {
			case BRICK:
			case ROCK:
			case LADDER:
				return true;
			default:
				return false;
		}
	}

	public static boolean isPassable(Coordinates coords, char[][] level) {
		if (coords == null)
			return false;
		
		return isPassable(level[coords.getY()][coords.getX()]);
	}
	
	public static boolean isWalkable(Coordinates coords, char[][] level) {
		if (coords == null)
			return false;
		
		return isWalkable(level[coords.getY()][coords.getX()]);
	}
}
