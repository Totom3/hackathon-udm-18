package us.astar.loderunner;

public class DigCoordinates extends Coordinates {

	private final boolean isLeft;

	public DigCoordinates(int x, int y, boolean isLeft) {
		super(x, y);
		this.isLeft = isLeft;
	}

	public boolean isLeft() {
		return isLeft;
	}

	public boolean isRight() {
		return !isLeft;
	}

	@Override
	public String toString() {
		if (isLeft) {
			return "[" + getX() + ", " + getY() + ")";
		}

		return "(" + getX() + ", " + getY() + "]";
	}
}
