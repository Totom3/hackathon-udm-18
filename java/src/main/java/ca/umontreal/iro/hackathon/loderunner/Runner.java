package ca.umontreal.iro.hackathon.loderunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import us.astar.loderunner.RunnerIntelligence;

/**
 *
 */
public class Runner extends BasicRunner {

	public static final String ROOM;

	private static final boolean USE_BUILTIN_ROOM = true;

	static {
		if (USE_BUILTIN_ROOM) {
			ROOM = "TheAStarTeam";
		} else {
			try {
				StringBuilder builder = new StringBuilder();
				Files.lines(new File("../key.txt").toPath()).forEach(builder::append);
				ROOM = builder.toString();
			} catch (IOException ex) {
				throw new AssertionError(ex);
			}
		}
	}

	public static final int START_LEVEL = 11;

	private RunnerIntelligence intelligence;

	public Runner() {
		super(ROOM, START_LEVEL);
	}

	@Override
	public void start(String[] grid) {
		System.out.println("New Level! ");

		int sizeY = grid.length;
		int sizeX = grid[0].length();
		char[][] levelGrid = new char[sizeY][sizeX];
		for (int y = 0; y < sizeY; ++y) {
			for (int x = 0; x < sizeX; ++x) {
				levelGrid[y][x] = grid[y].charAt(x);
			}
		}

		this.intelligence = new RunnerIntelligence(levelGrid);
		intelligence.parseLevel();
		intelligence.computePaths();
	}

	@Override
	public Move next(int x, int y) {
		return intelligence.move(x, y);
	}
}
